﻿using System;
using Gtk;

//Main window class. Created with Stetic GUI designer. 
//Boilerplate code which creates and packs widgets can be found in gtk-gui/MainWindow.cs
public partial class MainWindow: Gtk.Window {
	Gdk.Pixbuf pixbuf;
	Gdk.Pixmap pixmap;
	// Size of region for drawing (constant in our application)
	const int Width = 1600;
	const int Height = 800;

	public MainWindow()
		: base(Gtk.WindowType.Toplevel) {
		Build();
		//
		this.drawingarea1.AddEvents((int) Gdk.EventMask.PointerMotionMask);
		this.drawingarea1.MotionNotifyEvent += delegate(object motionObj, MotionNotifyEventArgs motionArgs) {
			Gdk.EventMotion motion = motionArgs.Event;
			//Console.WriteLine("Mouse Motion: moveTo {0},{1}", motion.X, motion.Y);
			//Print the pointer position on the statusbar
			var context = this.statusbar1.GetContextId("statusbar-ctx");
			this.statusbar1.Push(context, String.Format("{0}:{1}", motion.X, motion.Y));
		};
		Gdk.Color c = new Gdk.Color(0xe0, 0xe0, 0xe0);
		/* 1. Using Pixbuf */
		/*pixbuf = new Gdk.Pixbuf(Gdk.Colorspace.Rgb, false, 8, Width, Height);
		pixbuf.Fill(c.Pixel);*/
		/* 2. Using Pixmap instead */
		pixmap = new Gdk.Pixmap(drawingarea1.GdkWindow, Width, Height);
		Gdk.GC windowCtx = new Gdk.GC(drawingarea1.GdkWindow);
		windowCtx.RgbFgColor = c;
		Gdk.Rectangle rect = new Gdk.Rectangle(0, 0, Width, Height);
		pixmap.DrawRectangle(windowCtx, true, rect);
		drawingarea1.QueueDrawArea(rect.X, rect.Y, rect.Width, rect.Height);
	}

	protected void OnDeleteEvent(object sender, DeleteEventArgs a) {
		Application.Quit();
		a.RetVal = true;
	}

	//Expose event. Occured when there is a necessity of redrawing the window of Gtk.DrawingArea
	protected void OnDrawingarea1ExposeEvent(object o, ExposeEventArgs args) {
		//Console.WriteLine("Expose Event");
		int actualH, actualW;
		GdkWindow.GetSize(out actualW, out actualH);
		/*//1. When using Pixbuf
		//Render Pixbuf onto DrawingArea here
		Gtk.DrawingArea area = (Gtk.DrawingArea) o;
		Gdk.GC gc = new Gdk.GC(area.GdkWindow);
		pixbuf.RenderToDrawable(area.GdkWindow, gc, 0, 0, 0, 0, actualW, actualH, Gdk.RgbDither.None, 0, 0);
		*/
		//2. When using Pixmap
		Gtk.DrawingArea area = (Gtk.DrawingArea) o;
		area.GdkWindow.DrawDrawable(new Gdk.GC(area.GdkWindow), pixmap, 0, 0, 0, 0, actualW, actualH);
	}

	protected void OnDrawingarea1ConfigureEvent(object o, ConfigureEventArgs args) {
		DrawingArea area = o as DrawingArea;
		Console.WriteLine("Configure Event");
	}

	protected void OnFloppyActionActivated(object sender, EventArgs e) {
		//Create standard file saving dialog
		Gtk.FileChooserDialog dialog = new Gtk.FileChooserDialog("Save as", this, 
			                               FileChooserAction.Save, 
			                               Gtk.Stock.Cancel, Gtk.ResponseType.Cancel,
			                               Gtk.Stock.Ok, Gtk.ResponseType.Ok);
		//Run dialog and ask the result
		switch (dialog.Run()) {
			case (int) Gtk.ResponseType.Cancel:
				Console.WriteLine("Cancel");
				break;
			case (int) Gtk.ResponseType.Ok:
				try {
					string filename = dialog.Filename;
					//Saving the whole pixmap in file
					int width, height;
					pixmap.GetSize(out width, out height);
					Gdk.Pixbuf pixbufToSave = Gdk.Pixbuf.FromDrawable(pixmap,
						pixmap.Colormap, 0, 0, 0, 0, width, height);
					pixbufToSave.Save(filename, "png");
					Console.WriteLine("OK " + filename);
				}
				catch (Exception exc) {
					Console.WriteLine("ERROR: couldn't possible to save pixmap: " + exc.ToString());
				}
				break;
		}
		dialog.Destroy();
	}

	protected void OnColorPickerActionActivated(object sender, EventArgs e) {
		Console.WriteLine("Activated");
	}
	protected void MouseBtnPressed(object o, ButtonPressEventArgs args) {
		this.MotionNotifyEvent += DrawBrushEvent;
		Console.WriteLine("MouseButtonPressed");
	}
	protected void MouseBtnReleased(object o, ButtonReleaseEventArgs args) {
		this.MotionNotifyEvent -= DrawBrushEvent;
		Console.WriteLine("MouseButtonReleased");
		args.RetVal = false;
	}

	protected void OnColorPickerActionToggled(object sender, EventArgs e) {
		Gtk.ToggleAction action = sender as Gtk.ToggleAction;
		Console.WriteLine("Type=" + sender.GetType());
		if (action.Active) {
			//Add event handlers on mouse down and mouse up
			this.drawingarea1.AddEvents((int)(Gdk.EventMask.ButtonPressMask | Gdk.EventMask.ButtonReleaseMask));
			this.drawingarea1.ButtonPressEvent += MouseBtnPressed;
			this.drawingarea1.ButtonReleaseEvent += MouseBtnReleased;
			Console.WriteLine("Toggled");
		} else {
			//Remove event handlers
			this.drawingarea1.ButtonPressEvent -= MouseBtnPressed;
			this.drawingarea1.ButtonReleaseEvent -= MouseBtnReleased;
			Console.WriteLine("Un-Toggled");
		}
	}
	protected void OnExitAction1Activated(object sender, EventArgs e) {
		Application.Quit();
	}

	//Event-notification about mouse motion
	protected void DrawBrushEvent(object o, MotionNotifyEventArgs args) {
		Gdk.EventMotion e = args.Event;
		Gdk.Rectangle rect = new Gdk.Rectangle((int) e.X - 5,(int) e.Y - 5, 10, 10);
		/* 1. Direct drawing on window of Gtk.DrawingArea */
		/*Gdk.Window window = drawingarea1.GdkWindow;
		Gdk.GC gc = new Gdk.GC(window);
		gc.RgbFgColor = new Gdk.Color(0x50, 0x50, 0x50);
		window.DrawRectangle(gc, true, rect);*/
		/* 2. Using backing pixmap */
		Gdk.GC gc = new Gdk.GC(drawingarea1.GdkWindow);
		pixmap.DrawRectangle(gc, true, rect);
		drawingarea1.QueueDrawArea(rect.X, rect.Y, rect.Width, rect.Height);
		args.RetVal = false;
	}

}
