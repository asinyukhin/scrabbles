﻿using System;
using System.IO;
using Gtk;

namespace GtkExample
{
	class MainClass {
		public static void Main(string[] args) {
			//инициализируем Gtk-приложение
			Application.Init();
			//создаём в нём новое окно -- главное окно
			MainWindow win = new MainWindow();
			//отображаем окно
			win.Show();
			//запускаем приложение. здесь начинают отслеживаться события виджетов
			Application.Run();
		}
	}
}
